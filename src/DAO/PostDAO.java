package DAO;
import BlogApp.ArticlesPOJO;
import DAO.HikariConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostDAO implements AutoCloseable {

    private final Connection conn;

    public PostDAO() throws SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

//    public UserPOJO getUserById(int id) throws SQLException {
//        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM user WHERE id = ?")) {
//            stmt.setInt(1, id);
//            try (ResultSet rs = stmt.executeQuery()) {
//                if (rs.next()) {
//                    return userFromResultSet(rs);
//                } else {
//                    return null;
//                }
//            }
//        }
//    }
//
//
//    private UserPOJO userFromResultSet(ResultSet rs) throws SQLException {
//        return new UserPOJO(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9));
//    }




    public List<ArticlesPOJO> getAllArticles() throws SQLException {

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM article")) {
            try (ResultSet rs = stmt.executeQuery()) {
                List<ArticlesPOJO> articles = new ArrayList<>();
                while (rs.next()) {
                    articles.add(articleFromResultSet(rs));
                }
                return articles;
            }
        }
    }

    public ArticlesPOJO getArticleByUser(int authorId) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM articles WHERE authorId = ?")) {
            stmt.setInt(1, authorId);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return articleFromResultSet(rs);
                } else {
                    return null;
                }
            }
        }
    }


    public ArticlesPOJO getArticleById(int id) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM articles WHERE id = ?")) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return articleFromResultSet(rs);
                } else {
                    return null;
                }
            }
        }
    }


    private ArticlesPOJO articleFromResultSet(ResultSet rs) throws SQLException {
        return new ArticlesPOJO(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getDate(4), rs.getInt(5));
    }


    public void saveArticle(ArticlesPOJO article) throws SQLException {
        if (article.getId() == null) {
            addArticle(article);
        } else {
            updateArticle(article);
        }
    }


    public void addArticle(ArticlesPOJO article) throws SQLException {
        if (article.getId() == null) {
            addArticle_generateId(article);
        } else {
            addArticle_supplyId(article);
        }
    }


    private void addArticle_generateId(ArticlesPOJO article) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO post (title, content,dateCreated,authorID) VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1, article.getTitle());
            stmt.setString(2, article.getContent());
            stmt.setDate(3, (Date) article.getDateCreated());
            stmt.setInt(4,article.getAuthorId());
            stmt.executeUpdate();
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                rs.next();
                article.setId(rs.getInt(1));
            }
        }
    }


    private void addArticle_supplyId(ArticlesPOJO article) throws SQLException {

        if (article.getId() == null) {
            throw new IllegalArgumentException("article's id cannot be null.");
        }

        try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO post (id, title, content,dateCreated,authorID) VALUES (?,?,?,?,?)")) {
            stmt.setInt(1, article.getId());
            stmt.setString(2, article.getTitle());
            stmt.setString(3, article.getContent());
            stmt.setDate(4, (Date) article.getDateCreated());
            stmt.setInt(5,article.getAuthorId());
            stmt.executeUpdate();
        }
    }


    public void updateArticle(ArticlesPOJO article) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("UPDATE post SET title = ?, content = ? ,dateCreated = ? WHERE id = ?")) {
            stmt.setString(1, article.getTitle());
            stmt.setString(2, article.getContent());
            stmt.setDate(3, (Date) article.getDateCreated());
            stmt.setInt(4, article.getId());
            stmt.executeUpdate();
        }
    }

    /**
     * Deletes the article with the given id from the database, if any.
     */
    public void deleteArticle(int id) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM post WHERE id = ?")) {
            stmt.setInt(1, id);
            stmt.executeUpdate();
        }
    }

    @Override
    public void close() throws SQLException {
        this.conn.close();
    }

}
