package BlogApp;

import java.util.Date;

public class UserPOJO {
    private Integer id;
    private String fname;
    private String lname;
    private Date dateOfBirth;
    private String username;
    private String password;
    private String country;
    private String description;
    private String avatar;


    public UserPOJO(Integer id, String fname, String lname, Date dateOfBirth, String username, String password, String country, String description, String avatar){
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.dateOfBirth = dateOfBirth;
        this.username = username;
        this.password = password;

        this.country = country;
        this.description = description;
        this.avatar = avatar;
    }

    public Integer getId(){
        return id;
    }

    public String getFname(){
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }

    public String getCountry(){
        return country;
    }

    public Date getDateOfBirth(){
        return dateOfBirth;
    }

    public String getDescription(){
        return description;
    }

    public String getAvatar(){
        return avatar;
    }

    public void setId(int anInt){
        this.id = id;
    }

    public void setFname(String fname){
       this.fname = fname;
    }

    public void setLname(String lname){
        this.lname = lname;
    }

    public void setDateOfBirth(Date dateOfBirth){
        this.dateOfBirth = dateOfBirth;
    }


    public void setUsername(String username){
        this.username = username;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public void setCountry(String country){
        this.country = country;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setImage(String avatar){
        this.avatar = avatar;
    }



}
