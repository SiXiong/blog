drop table if exists user;
drop table if exists post;


create table user (
       id int not null,
       fname varchar(50),
       lname varchar(50),
       dateOfBirth date,
       username varchar (50),
       password varchar (16),
       country char (2),
       description varchar(1000),
       image varchar (100),
       primary key (id)
     );

insert into user (id, fname, lname, dateOfBirth, username, password, country, description, image) values
     (01, 'Harry', 'Potter', '2000-01-01','oooiii','1234','NZ','Harry Potter is a student wizard at Hogwarts ','src = lkj'),
     (02, 'Yarry', 'Totter', '2000-01-01','tooiii','1234','NZ','Harry Potter is a student wizard at Hogwarts ','src = lkj'),
     (03, 'Parry', 'Botter', '2000-01-01','qooiii','1234','NZ','Harry Potter is a student wizard at Hogwarts ','src = lkj')


create table post (
       id int not null,
       title varchar (100),
       contect varchar (2000),
       dateCreated date,
       authorID int not null,
       primary key (id),
       foreign key (authorID) references user(id)
     );

insert into post (id, title, contect, dateCreated, authorID) values
     (01, 'How we suffer with back end programming', 'Being stupid is hard, but train your brain and you can reach your back end programming goals :)', '2018-10-05', 01),
     (02, 'How we suffer with front end programming', 'Being stupid is hard, but train your brain and you can reach your from end programming goals :)', '2018-10-01', 01),
     (03, 'How we suffer with git', 'Being stupid is hard, but train your brain and you can reach your GIT programming goals :)', '2018-10-02', 02),
     (04, 'How we suffer with CSS', 'Being stupid is hard, but train your brain and you can reach your CSS programming goals :)', '2018-10-03', 03)


